# Breast Cancer Wisconsin Data Analysis

This project aims to classify breast cancer tumors as malignant or benign using logistic regression and k-nearest neighbors (KNN) models. The dataset used is the Breast Cancer Wisconsin (Diagnostic) Dataset, which contains features computed from digitized images of breast masses.

## Dataset

The Breast Cancer Wisconsin (Diagnostic) Dataset contains various features computed from digitized images of breast masses. Some of the features include:

- Mean radius
- Mean texture
- Mean perimeter
- Mean area
- Mean smoothness
- Mean compactness
- Mean concavity
- Mean concave points
- Mean symmetry
- Mean fractal dimension
- ... and more

The target variable is the diagnosis, where 'M' represents malignant and 'B' represents benign.

## Models Implemented

### Logistic Regression

Logistic regression is a statistical model used for binary classification. In this project, logistic regression is trained on the dataset to classify tumors as malignant or benign based on the provided features.

### K-Nearest Neighbors (KNN)

K-nearest neighbors is a non-parametric classification algorithm. It classifies data points based on the majority class of their k-nearest neighbors. In this project, KNN is applied to classify tumors using the features available in the dataset.

## Usage

To run the analysis and train the models:

1. Install the required dependencies by running `pip install -r requirements.txt`.
2. Run the `Simin_Logistic_v1.ipynb` Jupyter Notebook, which contains the code for data preprocessing, model training, and evaluation.

## Results

Both logistic regression and KNN models are evaluated based on accuracy, precision, recall, and F1-score metrics. The performance of each model is compared, and insights are provided regarding their effectiveness in classifying breast cancer tumors.

## Future Work

- Explore other classification algorithms such as decision trees, random forests, or support vector machines.
- Perform hyperparameter tuning to optimize the performance of the models.
- Investigate feature engineering techniques to improve classification accuracy.
- Deploy the best-performing model as a web application or API for real-time predictions.

## Contributors

- Simin Jawed